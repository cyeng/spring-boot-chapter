package org.minbox.chapter.springboot2.configuration.binding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

/**
 * 2.2.1.RELEASE版本属性绑定问题解决
 *
 * @author 恒宇少年
 */
@SpringBootApplication
@ConfigurationPropertiesScan
public class SpringbootConfigurationBindingBitPittedApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootConfigurationBindingBitPittedApplication.class, args);
    }

}
